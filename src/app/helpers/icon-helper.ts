export class IconHelper {
  public static readonly allIcons = Array(10)
    .fill(1)
    .map((_, index) => `p_${`${index + 1}`.padStart(2, '0')}`);

  private static readonly basePath = 'assets/Images';

  public static getIconPath(icon: string) {
    if (!IconHelper.allIcons.includes(icon) && this.allIcons.length > 0) {
      icon = this.allIcons[0];
    }

    return `${this.basePath}/${icon}.png`;
  }
}
