import { Component, OnInit, Input, Output, EventEmitter, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';
import { PlayerInfo } from '../models/player-info';
import { IconHelper } from '../helpers/icon-helper';
import { CounterComponent } from '../counter/counter.component';
import { debounceTime } from 'rxjs/operators';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-munchkin-counter-item',
  templateUrl: './munchkin-counter-item.component.html',
  styleUrls: ['./munchkin-counter-item.component.scss'],
})
export class MunchkinCounterItemComponent implements AfterViewInit, OnDestroy {
  private static readonly DEBOUNCE_TIME = 200;

  @Input()
  playerInfo: PlayerInfo;
  @Input()
  editModeActive: boolean;

  @Output()
  editClicked = new EventEmitter();
  @Output()
  removeClicked = new EventEmitter();
  @Output()
  edited = new EventEmitter<PlayerInfo>();

  @ViewChild('levelCounter')
  levelCounter: CounterComponent;
  @ViewChild('itemsCounter')
  itemsCounter: CounterComponent;

  private subscriptions = new Subscription();

  constructor() {}

  ngAfterViewInit() {
    this.subscriptions.add(
      this.levelCounter.valueChanged.pipe(debounceTime(MunchkinCounterItemComponent.DEBOUNCE_TIME)).subscribe((_) => {
        this.edited.emit(this.playerInfo);
      })
    );

    this.subscriptions.add(
      this.itemsCounter.valueChanged.pipe(debounceTime(MunchkinCounterItemComponent.DEBOUNCE_TIME)).subscribe((_) => {
        this.edited.emit(this.playerInfo);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  getIconPath(icon: string) {
    return IconHelper.getIconPath(icon);
  }
}
