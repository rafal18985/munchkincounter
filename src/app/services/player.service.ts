import { Injectable } from '@angular/core';
import { PlayerInfo } from '../models/player-info';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class PlayerService {
  players: PlayerInfo[] = [];

  players$: BehaviorSubject<PlayerInfo[]> = new BehaviorSubject<PlayerInfo[]>([]);
  constructor() {
    this.load();
    this.notify();
  }

  add(player: PlayerInfo) {
    this.players = [...this.players, player];
    this.saveAndNotify();
  }

  update(index: number, player: PlayerInfo) {
    if (index < this.players.length) {
      this.players = this.players.map((value, i) => {
        if (index !== i) {
          return value;
        } else {
          return player;
        }
      });
      this.saveAndNotify();
    }
  }

  remove(index: number) {
    if (index < this.players.length) {
      this.players = this.players.filter((value, i) => index !== i);
      this.saveAndNotify();
    }
  }

  private saveAndNotify() {
    this.save();
    this.notify();
  }

  private notify() {
    this.players$.next(this.players);
  }

  private save() {
    localStorage.setItem('players', JSON.stringify(this.players));
  }

  private load() {
    const playersJsonString = localStorage.getItem('players');
    if (playersJsonString !== null) {
      this.players = JSON.parse(playersJsonString);
    }
  }
}
