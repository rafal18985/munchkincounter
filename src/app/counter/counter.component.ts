import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.scss'],
})
export class CounterComponent {
  @Input()
  title: string;
  @Input()
  value: number;
  @Input()
  minValue: number;
  @Input()
  maxValue: number;

  @Output()
  valueChanged = new EventEmitter<number>();

  constructor() {}

  increment() {
    const newValue = this.value + 1;
    if (this.maxValue != null && newValue > this.maxValue) {
      return;
    }
    this.value = newValue;
    this.valueChanged.emit(this.value);
  }

  decrement() {
    const newValue = this.value - 1;
    if (this.minValue != null && newValue < this.minValue) {
      return;
    }
    this.value = newValue;
    this.valueChanged.emit(this.value);
  }
}
