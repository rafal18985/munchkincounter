import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { PlayerInfo } from '../models/player-info';
import { IconHelper } from '../helpers/icon-helper';
import { IconDialogComponent } from '../icon-dialog/icon-dialog.component';

@Component({
  selector: 'app-player-dialog',
  templateUrl: './player-dialog.component.html',
  styleUrls: ['./player-dialog.component.scss'],
})
export class PlayerDialogComponent {
  constructor(
    public dialogRef: MatDialogRef<PlayerDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: PlayerInfo,
    public dialog: MatDialog
  ) {}

  getIconPath(icon: string) {
    return IconHelper.getIconPath(icon);
  }

  editIcon() {
    const dialogRef = this.dialog.open(IconDialogComponent, {
      data: this.data.icon,
      panelClass: 'dialog-background',
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result != null) {
        this.data.icon = result;
      }
    });
  }
}
