import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PlayerInfo } from '../models/player-info';

@Component({
  selector: 'app-munchkin-counter-list',
  templateUrl: './munchkin-counter-list.component.html',
  styleUrls: ['./munchkin-counter-list.component.scss'],
})
export class MunchkinCounterListComponent {
  @Input()
  players: PlayerInfo[];
  @Input()
  editModeActive: boolean;

  @Output()
  editPlayer = new EventEmitter<{ index: number; player: PlayerInfo }>();
  @Output()
  editedPlayer = new EventEmitter<{ index: number; player: PlayerInfo }>();
  @Output()
  removePlayer = new EventEmitter<number>();

  constructor() {}
}
