import { Component, OnInit, OnDestroy } from '@angular/core';
import { PlayerInfo } from '../models/player-info';
import { MatDialog } from '@angular/material/dialog';
import { PlayerDialogComponent } from '../player-dialog/player-dialog.component';
import { PlayerService } from '../services/player.service';
import { Observable, Subscription } from 'rxjs';
import { map, filter, pairwise } from 'rxjs/operators';
import { WinDialogComponent } from '../win-dialog/win-dialog.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit, OnDestroy {
  editModeActive = true;
  players$: Observable<PlayerInfo[]>;

  private subscriptions = new Subscription();

  constructor(public dialog: MatDialog, private playerService: PlayerService) {}

  ngOnInit() {
    this.players$ = this.playerService.players$;

    const winner$ = this.players$.pipe(
      map((players) => players.find((player) => player.level === 10)),
      pairwise(),
      filter((pair) => {
        if (pair[0] !== undefined || pair[1] === undefined) {
          return false;
        }

        return true;
      }),
      map((pair) => pair[1])
    );

    this.subscriptions.add(
      winner$.subscribe((winner) => {
        this.openWinDialog(winner);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  toogleEditMode() {
    this.editModeActive = !this.editModeActive;
  }

  openWinDialog(winner: PlayerInfo) {
    this.dialog.open(WinDialogComponent, {
      data: { ...winner },
      panelClass: 'dialog-background',
    });
  }

  openAddDialog() {
    const dialogRef = this.dialog.open(PlayerDialogComponent, {
      data: { name: 'Player', race: 'human', sex: 'male', level: 1, itemsPower: 0 },
      panelClass: 'dialog-background',
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result != null) {
        this.playerService.add(result);
      }
    });
  }

  openEditDialog(index: number, player: PlayerInfo) {
    const dialogRef = this.dialog.open(PlayerDialogComponent, {
      data: { ...player },
      panelClass: 'dialog-background',
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result != null) {
        this.edit(index, result);
      }
    });
  }

  edit(index: number, player: PlayerInfo) {
    this.playerService.update(index, player);
  }

  remove(index: number) {
    this.playerService.remove(index);
  }
}
