import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';

import { CounterComponent } from './counter/counter.component';
import { MunchkinCounterItemComponent } from './munchkin-counter-item/munchkin-counter-item.component';
import { MunchkinCounterListComponent } from './munchkin-counter-list/munchkin-counter-list.component';
import { HomeComponent } from './home/home.component';
import { PlayerDialogComponent } from './player-dialog/player-dialog.component';
import { IconDialogComponent } from './icon-dialog/icon-dialog.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { WinDialogComponent } from './win-dialog/win-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    MunchkinCounterItemComponent,
    CounterComponent,
    MunchkinCounterListComponent,
    HomeComponent,
    PlayerDialogComponent,
    IconDialogComponent,
    WinDialogComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MatCardModule,
    MatButtonModule,
    MatIconModule,
    MatToolbarModule,
    MatDialogModule,
    MatInputModule,
    MatFormFieldModule,
    MatSelectModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
  ],
  entryComponents: [PlayerDialogComponent, IconDialogComponent, WinDialogComponent],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
