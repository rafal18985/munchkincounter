export interface PlayerInfo {
  name: string;
  icon: string;
  level: number;
  itemsPower: number;
}
