import { Component, OnInit, Inject } from '@angular/core';
import { PlayerInfo } from '../models/player-info';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-win-dialog',
  templateUrl: './win-dialog.component.html',
  styleUrls: ['./win-dialog.component.scss'],
})
export class WinDialogComponent {
  constructor(public dialogRef: MatDialogRef<WinDialogComponent>, @Inject(MAT_DIALOG_DATA) public winner: PlayerInfo) {}
}
