import { Component, Inject } from '@angular/core';
import { IconHelper } from '../helpers/icon-helper';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PlayerDialogComponent } from '../player-dialog/player-dialog.component';

@Component({
  selector: 'app-icon-dialog',
  templateUrl: './icon-dialog.component.html',
  styleUrls: ['./icon-dialog.component.scss'],
})
export class IconDialogComponent {
  allIcons = IconHelper.allIcons;

  constructor(public dialogRef: MatDialogRef<PlayerDialogComponent>, @Inject(MAT_DIALOG_DATA) public selectedIcon: string) {}

  getIconPath(icon: string) {
    return IconHelper.getIconPath(icon);
  }
}
